from datetime import datetime
from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return HttpResponse(
        "Hello, world. You're at the polls index: " + add_integer_sequence())


def hello_there(request, name):
    now = datetime.now()
    formatted_now = now.strftime("%A, %d %B, %Y at %X")

    content = "Hello there, " + name + "! It's " + formatted_now

    return HttpResponse(content)


def add_integer_sequence():
    result = ''
    for i in range(20):
        result += str(i)

    return result
